project('https-customs', 'rust',
          version: '0.1.0',
    meson_version: '>= 0.55',
)

gettext_package = meson.project_name()

# Create a base application id
base_app_id = 'xyz.tytanium.HTTPSCustoms'

# List features
available_features = [ 'url_cleaner', 'frontend_changer', 'url_unshortener' ]

version = meson.project_version()
version_array = version.split('.')
major_version = version_array[0].to_int()
minor_version = version_array[1].to_int()
version_micro = version_array[2].to_int()

i18n = import('i18n')
gnome = import('gnome')

dependency('glib-2.0', version: '>= 2.56')
dependency('gio-2.0', version: '>= 2.56')
dependency('gdk-pixbuf-2.0')
dependency('gtk4', version: '>= 4')
dependency('libadwaita-1', version: '>= 1.1')

cargo = find_program('cargo', required: true)
xsltproc = find_program('xsltproc', required: true)
gresource = find_program('glib-compile-resources', required: true)
gschemas = find_program('glib-compile-schemas', required: true)
#cargo_vendor = find_program('cargo-vendor', required: false)
cargo_script = find_program('build-aux/cargo.sh')

prefix = get_option('prefix')
bindir = join_paths(prefix, get_option('bindir'))
localedir = join_paths(prefix, get_option('localedir'))

datadir = join_paths(prefix, get_option('datadir'))
pkgdatadir = join_paths(datadir, meson.project_name())
iconsdir = join_paths(datadir, 'icons')
podir = join_paths(meson.source_root(), 'po')

if get_option('profile') == 'development'
  profile = 'Devel'
  name_suffix = ' Development'
  vcs_tag = run_command('git', 'rev-parse', '--short', 'HEAD').stdout().strip()
  if vcs_tag == ''
    version_suffix = '-devel'
  else
    version_suffix = '-devel-@0@'.format(vcs_tag)
  endif
elif get_option('profile') == 'nightly'
  profile = 'Nightly'
  name_suffix = ' Nightly'
  vcs_tag = run_command('git', 'rev-parse', '--short', 'HEAD').stdout().strip()
  if vcs_tag == ''
    version_suffix = '-nightly'
  else
    version_suffix = '-nightly-@0@'.format(vcs_tag)
  endif
else
  profile = ''
  name_suffix = ''
  version_suffix = ''
endif

# App profile to the application id
application_id = base_app_id + profile

# App path for gschema & gresource file
app_path_components = application_id.split('.')
application_path = '/'
foreach i : app_path_components
  application_path = application_path / i
endforeach
application_path = application_path + '/'

subdir('data')
subdir('po')

subdir('https_customs/src')
subdir('src')

meson.add_dist_script(
  'build-aux/dist-vendor.sh',
  meson.source_root(),
  join_paths(meson.build_root(), 'meson-dist', meson.project_name() + '-' + version)
)

meson.add_install_script('build-aux/meson_postinstall.py')

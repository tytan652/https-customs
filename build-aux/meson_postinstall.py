#!/usr/bin/env python3

from os import environ, path
from subprocess import call

prefix = environ.get('MESON_INSTALL_PREFIX', '/usr/local')
datadir = path.join(prefix, 'share')

# Package managers set this so we don't need to run
if not environ.get('DESTDIR', ''):
    PREFIX = environ.get('MESON_INSTALL_PREFIX', '/usr/local')
    DATA_DIR = path.join(PREFIX, 'share')
    print('Updating icon cache...')
    call(['gtk-update-icon-cache', '-qtf', path.join(DATA_DIR, 'icons', 'hicolor')])

    print('Compiling GSettings schemas...')
    call(['glib-compile-schemas', path.join(DATA_DIR, 'glib-2.0', 'schemas')])

    print('Updating desktop database...')
    call(['update-desktop-database', '-q', path.join(DATA_DIR, 'applications')])




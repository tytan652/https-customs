#!/bin/sh

export MESON_BUILD_ROOT="$1"
export MESON_SOURCE_ROOT="$2"
export CARGO_TARGET_DIR="$MESON_BUILD_ROOT"/target
export CARGO_HOME="$CARGO_TARGET_DIR"/cargo-home
export OUTPUT="$3"
export APP_BIN="$4"
export PROFILE="$5"

if [ "$6" ]; then
    export FEATURES=--features"$6"
    echo "ENABLED FEATURES:$6"
else
    export FEATURES=""
    echo "NO FEATURES ENABLED"
fi

if  [ "$PROFILE" = "Devel" ]
then
    echo "DEBUG MODE"
    cargo build --manifest-path \
        "$MESON_SOURCE_ROOT"/Cargo.toml --verbose $FEATURES && \
        cp "$CARGO_TARGET_DIR"/debug/"$APP_BIN" "$OUTPUT"
else
    echo "RELEASE MODE"
    cargo build --manifest-path \
        "$MESON_SOURCE_ROOT"/Cargo.toml --release $FEATURES && \
        cp "$CARGO_TARGET_DIR"/release/"$APP_BIN" "$OUTPUT"
fi

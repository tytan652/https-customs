use url::Url;

use gtk::gio;
use gtk::prelude::*;

mod config;
pub mod settings;

#[cfg(feature = "frontend_changer")]
mod frontend_checks;
#[cfg(feature = "frontend_changer")]
use crate::{
    settings::frontend_changer_settings,
    settings::frontend_common_settings,
    settings::FrontEndChangerKey,
    frontend_checks::*
};
#[cfg(feature = "url_unshortener")]
use reqwest::{
    blocking::*,
    redirect::Policy,
    header::LOCATION
};
#[cfg(feature = "url_unshortener")]
use crate::settings::url_unshortener_settings;

#[cfg(feature = "url_unshortener")]
fn unshort_url(mut url: Url) -> Url {
    let mut domains: Vec<&str> = Vec::new();
    domains.push("t.co");
    domains.push("nyti.ms");
    domains.push("bit.ly");
    domains.push("amp.gs");
    domains.push("tinyurl.com");
    domains.push("goo.gl");
    domains.push("nzzl.us");
    domains.push("ift.tt");
    domains.push("ow.ly");
    domains.push("bl.ink");
    domains.push("buff.ly");
    domains.push("maps.app.goo.gl");


    if domains.contains(&url.host_str().unwrap()) {
        let client = ClientBuilder::new()
            .redirect(Policy::none())
            .build()
            .unwrap();
        while domains.contains(&url.host_str().unwrap()) {
            let request = client.head(url.clone()).send(); //TODO: add retry ?
            if request.is_ok() { // If request goes wrong not managed
                let response = request.unwrap();
                if response.headers().contains_key(LOCATION) {
                    let location = response.headers().get(LOCATION).unwrap();
                    let location_url = Url::parse(location.to_str().unwrap());
                    if location_url.is_ok() {
                        url = location_url.unwrap();
                    }
                }
            }
        } // Loop end
    }

    url
}

pub fn monitor_url(url: &str) -> String {

    let result_url = Url::parse(url);
    if result_url.is_err() {
        return String::from(url);
    }
    else {
        let mut parsed_url = result_url.unwrap();

        #[cfg(feature = "url_unshortener")]
        {
            if url_unshortener_settings::get_enabled() {
                parsed_url = unshort_url(parsed_url);
            }
        }

        #[cfg(feature = "url_cleaner")]
        {

        }

        #[cfg(feature = "frontend_changer")]
        {
            if frontend_changer_settings::get_enabled() {
                // YouTube
                if frontend_common_settings::get_enabled(FrontEndChangerKey::YouTube) {
                    parsed_url = youtube_check(parsed_url);
                }

                // Twitter
                if frontend_common_settings::get_enabled(FrontEndChangerKey::Twitter) {
                    parsed_url = twitter_check(parsed_url);
                }

                // Instagram
                if frontend_common_settings::get_enabled(FrontEndChangerKey::Instagram) {
                    parsed_url = instagram_check(parsed_url);
                }

                // Reddit
                if frontend_common_settings::get_enabled(FrontEndChangerKey::Reddit) {
                    parsed_url = reddit_check(parsed_url);
                }

                // Google Maps
                if frontend_common_settings::get_enabled(FrontEndChangerKey::GoogleMaps) {
                    parsed_url = maps_check(parsed_url);
                }
            }
        }

        return parsed_url.into_string();
    }
}

pub fn monitor_urls_in_gfiles(urls: &[gio::File]) -> Vec<String> {
    let mut monitored_urls: Vec<String> = Vec::new();
    for url in urls {
        if url.has_uri_scheme("http") || url.has_uri_scheme("https") {
            let url_to_monitor = url.get_uri();
            let monitored_url = monitor_url(&url_to_monitor);
            monitored_urls.push(monitored_url);
        }
    }

    monitored_urls
}

use url::{
    Url,
    form_urlencoded::byte_serialize
};
use regex::Regex;

use crate::settings::{
    frontend_common_settings,
    FrontEndChangerKey
};

pub fn youtube_check(mut url: Url) -> Url {

    let mut yt_urls: Vec<&str> = Vec::new();
    yt_urls.push("youtube.com");
    yt_urls.push("www.youtube.com");
    yt_urls.push("m.youtube.com");
    yt_urls.push("youtube-cookie.com");

    if frontend_common_settings::get_use_frontend(FrontEndChangerKey::YouTube) {
        let iv_instance = frontend_common_settings::get_frontend_url(FrontEndChangerKey::YouTube);

        if url.host_str().unwrap() == "youtu.be" {
            let mut youtube_vid = String::from(url.path());
            youtube_vid.remove(0); // remove the base / in the path

            url.set_path("/watch");

            if url.query().unwrap_or("notime").starts_with("t=") {
                let url_clone = url.clone();
                let time = url_clone.query().unwrap().strip_prefix("t=").unwrap();
                url.query_pairs_mut()
                   .clear()
                   .append_pair("v", &youtube_vid)
                   .append_pair("t", time);
            }
            else {
                url.query_pairs_mut()
                   .clear()
                   .append_pair("v", &youtube_vid);
            }

            url.set_host(iv_instance.host_str()).ok(); // Log this ?
            }
        else if yt_urls.contains(&url.host_str().unwrap()) {
            url.set_host(iv_instance.host_str()).ok(); // Log this ?
        }
    }
    url
}

pub fn twitter_check(mut url: Url) -> Url {

    let mut tw_urls: Vec<&str> = Vec::new();
    tw_urls.push("twitter.com");
    tw_urls.push("mobile.twitter.com");
    tw_urls.push("www.twitter.com");

    if frontend_common_settings::get_use_frontend(FrontEndChangerKey::Twitter) {
        let nitter_instance = frontend_common_settings::get_frontend_url(FrontEndChangerKey::Twitter);

        if url.host_str().unwrap() == "pbs.twimg.com" || url.host_str().unwrap() == "pic.twitter.com" {
            let mut new_url = nitter_instance.clone();
            let url_encoded: String = byte_serialize(url.as_str().as_bytes()).collect();
            new_url.set_path(&format!("/pic/{}", &url_encoded));
            url = new_url.clone();
        }
        else if tw_urls.contains(&url.host_str().unwrap()) {
            url.set_host(nitter_instance.host_str()).ok(); // Log this ?

            if url.path().ends_with("/tweets") {
                let new_path = url.path().strip_suffix("/tweets").unwrap().to_string();
                url.set_path(&new_path);
            }
        }
    }
    url
}

pub fn instagram_check(mut url: Url) -> Url {

    let mut ig_urls: Vec<&str> = Vec::new();
    ig_urls.push("instagram.com");
    ig_urls.push("www.instagram.com");
    ig_urls.push("m.instagram.com");

    if frontend_common_settings::get_use_frontend(FrontEndChangerKey::Instagram) {
        let bg_instance = frontend_common_settings::get_frontend_url(FrontEndChangerKey::Instagram);

        if ig_urls.contains(&url.host_str().unwrap()) {
            url.set_host(bg_instance.host_str()).ok(); // Log this ?

            if url.path().starts_with("/tv/") || url.path().starts_with("/igtv/") || url.path().starts_with("/reel/") {
                url.set_query(None);
            }
            else if !(url.path().starts_with("/privacy")) && !(url.path().starts_with("/p/")) {
                let path = url.path().to_string();
                url.set_path(&format!("/u{}", &path));
            }
        }
    }

    url
}

pub fn reddit_check(mut url: Url) -> Url {
    let mut reddit_urls: Vec<&str> = Vec::new();
    reddit_urls.push("reddit.com");
    reddit_urls.push("www.reddit.com");
    reddit_urls.push("i.reddit.com");

    if frontend_common_settings::get_use_frontend(FrontEndChangerKey::Reddit) {
        let teddit_instance = frontend_common_settings::get_frontend_url(FrontEndChangerKey::Reddit);

        if url.host_str().unwrap() == "preview.redd.it" || url.host_str().unwrap() == "i.redd.it" {
            url.set_query(None); // Mostly for preview.redd.it
            let mut image = String::from(url.path());
            image.remove(0); // remove the base / in the path

            url.set_path(&format!("/pics/w:null_{}", &image));

            url.set_host(teddit_instance.host_str()).ok(); // Log this ?
        }
        else if reddit_urls.contains(&url.host_str().unwrap()) {
            url.set_host(teddit_instance.host_str()).ok(); // Log this ?
        }
    }

    url
}

pub fn maps_check(mut url: Url) -> Url {
    let google_regex = Regex::new(r"^(|www\.)google\..*").unwrap();

    if frontend_common_settings::get_use_frontend(FrontEndChangerKey::GoogleMaps) {
        if url.host_str().unwrap() == "maps.google.com" {
            if url.query().unwrap().starts_with("ll=") && url.query_pairs().count() == 1 { // Only latitude and longitude is given
                // Like https://maps.google.com/?ll=38.882147,-76.99017
                let mut coords = url.query().unwrap().to_string(); // ll=38.882147,-76.99017
                coords = coords.strip_prefix("ll=").unwrap().to_string(); // 38.882147,-76.99017
                let split_coords: Vec<&str> = coords.split(',').collect();
                let lat = split_coords[0].parse::<f64>().unwrap(); // 38.882147
                let long = split_coords[1].parse::<f64>().unwrap(); // -76.99017

                url = osm_coord(lat, long, None);
            }
        }
        else if google_regex.is_match(url.host_str().unwrap()) && url.path().starts_with("/maps/") {
            if url.path().starts_with("/maps/@") {
                if url.path() == "/maps/@" && url.query().is_some() { // There is only a query -> API call
                    // Like https://www.google.com/maps/@?api=1&map_action=map&center=-33.712206,150.311941&zoom=12&basemap=terrain
                        if url.query().unwrap().starts_with("api=1&map_action=map&center=") && url.query_pairs().count() == 5 {
                            let mut query = url.query().unwrap().to_string(); // api=1&map_action=map&center=-33.712206,150.311941&zoom=12&basemap=terrain
                            query = query.strip_prefix("api=1&map_action=map&center=").unwrap().to_string(); // -33.712206,150.311941&zoom=12&basemap=terrain
                            let split_query: Vec<&str> = query.split('&').collect();
                            let split_coords: Vec<&str> = split_query[0].split(',').collect(); // -33.712206,150.311941
                            let zoom = split_query[1].strip_prefix("zoom=").unwrap(); // "12" //FIXME: May broke if no zoom is possible

                            let lat = split_coords[0].parse::<f64>().unwrap(); // -33.712206
                            let long = split_coords[1].parse::<f64>().unwrap(); // 150.311941

                            url = osm_coord(lat, long, Some(zoom.parse::<i32>().unwrap()));
                        }
                }
                else { // Maybe it is only coordinate in the path
                    // Like https://www.google.com/maps/@48.8583701,2.2922926,17z?hl=en
                    let mut path = String::from(url.path()); // /maps/@48.8583701,2.2922926,17z
                    path = path.strip_prefix("/maps/@").unwrap().to_string(); // 48.8583701,2.2922926,17z

                    let split_path: Vec<&str> = path.split(',').collect();
                    if split_path.len() == 3 { // lat,long,zoom
                        let lat = split_path[0].parse::<f64>().unwrap(); // 48.8583701
                        let long = split_path[1].parse::<f64>().unwrap(); // 2.2922926
                        let mut zoom = split_path[2].to_string(); // "17z"
                        zoom = zoom.strip_suffix("z").unwrap().to_string(); // "17"

                        url = osm_coord(lat, long, Some(zoom.parse::<i32>().unwrap()));
                    }
                }
            }
            else if url.path().starts_with("/maps/place/") {
                // Like https://www.google.ca/maps/place/1200+Pennsylvania+Ave+SE,+Washington,+DC+20003/
                // Or like https://www.google.com/maps/place/1200 Pennsylvania Ave SE, Washington, District of Columbia, 20003
                let mut path = String::from(url.path());
                path = path.strip_prefix("/maps/place/").unwrap().to_string();
                if path.ends_with("/") {
                    path = path.strip_suffix("/").unwrap().to_string();
                }

                url = osm_search(&path);
            }
            else if url.path().starts_with("/maps/search/") {
                // Like https://www.google.com/maps/search/?api=1&query=1200%20Pennsylvania%20Ave%20SE%2C%20Washington%2C%20District%20of%20Columbia%2C%2020003
                // Or like https://www.google.com/maps/search/?api=1&query=47.5951518,-122.3316393
                if url.query().unwrap().starts_with("api=1&query=") && url.query_pairs().count() == 2 { // Only api and query
                                                                                      let mut query = url.query().unwrap().to_string();
                    query = query.strip_prefix("api=1&query=").unwrap().to_string();

                    url = osm_search(&query);
                }
            }
        }
    }

    url
}

fn osm_coord(lat: f64, long: f64, mut zoom: Option<i32>) -> Url {
    let mut url = frontend_common_settings::get_frontend_url(FrontEndChangerKey::GoogleMaps);

    if zoom.is_none() {
        zoom = Some(12) //Default zoom of OSM if we use something like this https://www.openstreetmap.org/?mlat=38.882147&mlon=-76.99017
    }
    url.set_fragment(Some(&format!("map={}/{}/{}/", zoom.unwrap(), lat, long)));

    url
}

fn osm_search(query: &str) -> Url {
    let mut url = frontend_common_settings::get_frontend_url(FrontEndChangerKey::GoogleMaps);
    url.set_path("search");
    url.set_query(Some(&format!("query={}", query)));

    url
}

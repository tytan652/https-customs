use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use url::Url;

use crate::config::APP_ID;
use crate::settings::FrontEndChangerKey;
use crate::settings::FrontEndCommonKey;

fn get_settings(frontend_key: FrontEndChangerKey) -> gio::Settings {
    let app_id = &format!("{}.FrontEndChanger.{}", APP_ID, frontend_key.to_string());
    gio::Settings::new(app_id)
}

pub fn bind_property<P: IsA<glib::Object>>(frontend_key: FrontEndChangerKey, key: FrontEndCommonKey, object: &P, property: &str) {
    let settings = get_settings(frontend_key);
    settings.bind(key.to_string().as_str(), object, property).flags(gio::SettingsBindFlags::DEFAULT).build();
}

fn get_boolean(frontend_key: FrontEndChangerKey, key: FrontEndCommonKey) -> bool {
    let settings = get_settings(frontend_key);
    settings.get_boolean(&key.to_string())
}

pub fn get_enabled(frontend_key: FrontEndChangerKey) -> bool {
    get_boolean(frontend_key, FrontEndCommonKey::Enabled)
}

pub fn get_use_frontend(frontend_key: FrontEndChangerKey) -> bool {
    get_boolean(frontend_key, FrontEndCommonKey::UseFrontend)
}

pub fn get_frontend_url(frontend_key: FrontEndChangerKey) -> Url {
    let settings = get_settings(frontend_key);
    Url::parse(settings.get_string(&FrontEndCommonKey::FrontendInstance.to_string()).unwrap().as_str()).unwrap()
}

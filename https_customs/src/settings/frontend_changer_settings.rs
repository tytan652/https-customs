use gtk::gio;
use gtk::glib;
use gtk::prelude::*;

use crate::config::APP_ID;

fn get_settings() -> gio::Settings {
    let app_id = &format!("{}.FrontEndChanger", APP_ID);
    gio::Settings::new(app_id)
}

pub fn bind_enabled_property<P: IsA<glib::Object>>(object: &P, property: &str, flags: gio::SettingsBindFlags) {
    let settings = get_settings();
    settings.bind("enabled", object, property).flags(flags).build();
}

pub fn get_enabled() -> bool {
    let settings = get_settings();
    settings.get_boolean("enabled")
}

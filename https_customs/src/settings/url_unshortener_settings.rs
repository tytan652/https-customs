use gtk::gio;
use gtk::glib;
use gtk::prelude::*;

use crate::config::APP_ID;
use crate::settings::UrlUnshortenerKey;

fn get_settings() -> gio::Settings {
    let app_id = &format!("{}.UrlUnshortener", APP_ID);
    gio::Settings::new(app_id)
}

pub fn bind_property<P: IsA<glib::Object>>(key: UrlUnshortenerKey, object: &P, property: &str) {
    let settings = get_settings();
    settings.bind(key.to_string().as_str(), object, property).flags(gio::SettingsBindFlags::DEFAULT).build();
}

pub fn get_enabled() -> bool {
    let settings = get_settings();
    settings.get_boolean("enabled")
}

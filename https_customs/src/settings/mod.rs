mod key;
pub mod general_settings;

pub use key::Key;

#[cfg(feature = "url_cleaner")]
pub mod url_cleaner_settings;

#[cfg(feature = "url_cleaner")]
pub use key::UrlCleanerKey;

#[cfg(feature = "frontend_changer")]
pub mod frontend_changer_settings;
#[cfg(feature = "frontend_changer")]
pub mod frontend_common_settings;

#[cfg(feature = "frontend_changer")]
pub use {
    key::FrontEndChangerKey,
    key::FrontEndCommonKey
};

#[cfg(feature = "url_unshortener")]
pub use key::UrlUnshortenerKey;

#[cfg(feature = "url_unshortener")]
pub mod url_unshortener_settings;

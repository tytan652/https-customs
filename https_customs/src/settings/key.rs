use strum_macros::{Display, EnumString};

#[derive(Display, Debug, Clone, EnumString)]
#[strum(serialize_all = "kebab_case")]
pub enum Key {
    DefaultHandler,
}

#[cfg(feature = "url_cleaner")]
#[derive(Display, Debug, Clone, EnumString)]
#[strum(serialize_all = "kebab_case")]
pub enum UrlCleanerKey {
    Enabled,
    //Exceptions,
}

#[cfg(feature = "frontend_changer")]
#[derive(Display, Debug, Clone, EnumString)]
#[strum(serialize_all = "PascalCase")]
pub enum FrontEndChangerKey {
    YouTube,
    Twitter,
    Instagram,
    Reddit,
    GoogleMaps,
}

#[cfg(feature = "frontend_changer")]
#[derive(Display, Debug, Clone, EnumString)]
#[strum(serialize_all = "kebab_case")]
pub enum FrontEndCommonKey {
    Enabled,
    //UseDefaultHandler,
    //CustomHandler,
    UseFrontend,
    //ChangeEvenAlready,
    FrontendInstance
}

#[cfg(feature = "url_unshortener")]
#[derive(Display, Debug, Clone, EnumString)]
#[strum(serialize_all = "kebab_case")]
pub enum UrlUnshortenerKey {
    Enabled,
}

use gtk::gio;
use gtk::glib;
use gtk::prelude::*;

use crate::config::APP_ID;
use log::error;
use crate::settings::Key;

fn get_settings() -> gio::Settings {
    gio::Settings::new(APP_ID)
}

pub fn bind_property<P: IsA<glib::Object>>(key: Key, object: &P, property: &str) {
    let settings = get_settings();
    settings.bind(key.to_string().as_str(), object, property).flags(gio::SettingsBindFlags::DEFAULT).build();
}

pub fn get_string(key: Key) -> String {
    let settings = get_settings();
    settings.get_string(&key.to_string()).unwrap().to_string()
}

pub fn set_string(key: Key, value: String) {
    let settings = get_settings();
    if let Err(err) = settings.set_string(&key.to_string(), &value)
    {
        error!("Failed to save {} setting due to {}", key.to_string(), err);
    }
}

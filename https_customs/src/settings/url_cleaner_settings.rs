use gtk::gio;
use gtk::glib;
use gtk::prelude::*;

use crate::config::APP_ID;
use crate::settings::UrlCleanerKey;

fn get_settings() -> gio::Settings {
    let app_id = &format!("{}.UrlCleaner", APP_ID);
    gio::Settings::new(app_id)
}

pub fn bind_property<P: IsA<glib::Object>>(key: UrlCleanerKey, object: &P, property: &str) {
    let settings = get_settings();
    settings.bind(key.to_string().as_str(), object, property).flags(gio::SettingsBindFlags::DEFAULT).build();
}

option (
  'profile',
  type: 'combo',
  choices: [
    'release',
    'nightly',
    'development'
  ],
  value: 'nightly'
)

option (
  'flatpak',
  type: 'boolean',
  value: false,
  description: 'Enable flatpak related code'
)

option (
  'url_cleaner',
  type: 'boolean',
  value: false,
  description: 'Enable URL cleaner functions'
)

option (
  'frontend_changer',
  type: 'boolean',
  value: true,
  description: 'Enable front-end changer functions'
)

option (
  'url_unshortener',
  type: 'boolean',
  value: false,
  description: 'Enable URL unshortener functions'
)

option (
  'yt_alt_instance',
  type: 'string',
  value: 'https://invidious.snopyta.org/',
  description: 'Set default YouTube front-end alternative instance'
)

option (
  'twitter_alt_instance',
  type: 'string',
  value: 'https://nitter.snopyta.org/',
  description: 'Set default Twitter front-end alternative instance'
)

option (
  'ig_alt_instance',
  type: 'string',
  value: 'https://bibliogram.snopyta.org/',
  description: 'Set default Instagram front-end alternative instance'
)

option (
  'reddit_alt_instance',
  type: 'string',
  value: 'https://teddit.net/',
  description: 'Set default Reddit front-end alternative instance'
)

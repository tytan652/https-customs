use gtk::glib::{Receiver, Sender, clone};
use gtk::glib;
use std::{cell::RefCell, rc::Rc};
use gtk::gio;
use gtk::prelude::*;
use gtk_macros::{action, get_widget};
use gettextrs::*;
use https_customs::*;
use https_customs::settings::{Key, general_settings};
#[cfg(feature = "flatpak")]
use ashpd::desktop::open_uri::{OpenURIProxy, OpenFileOptions};
#[cfg(feature = "flatpak")]
use ashpd::WindowIdentifier;

use crate::{
    ui::SettingsWindow,
    ui::HandlersWindow,
    config,
    common
};

pub enum Action {
    ChangeHandler(String),
}

pub struct Application {
    app: gtk::Application,
    window: SettingsWindow,
    sender: Sender<Action>,
    receiver: RefCell<Option<Receiver<Action>>>,
}

macro_rules! none_handler {
    ($notif:expr, $app:expr) => {{
        $notif.set_priority(gio::NotificationPriority::High);
        $notif.set_body(Some(&gettext("No Handler set. Please open HTTPS Customs and set a handler")));
        $app.send_notification(Some("handler-none"), &$notif);
    }};
}

macro_rules! handler_in_list {
    ($handler:expr, $list:expr, $urls:expr, $notif:expr, $app:expr) => {{
        let handler = $list.get(&$handler).unwrap().to_owned();
        let result = handler.launch_uris($urls.as_slice(), Option::<&gio::AppLaunchContext>::None);

        if result.is_err() {
            $notif.set_priority(gio::NotificationPriority::High);
            $notif.set_body(Some(&gettext("An unexpected error was caught")));
            $app.send_notification(Some("error-launch-uris"), &$notif);
        }
    }};
}

macro_rules! no_url {
    ($notif:expr, $app:expr) => {{
        $notif.set_body(Some(&gettext("No valid URL(s) was given")));
        $app.send_notification(Some("no-valid-url"), &$notif);
    }};
}

impl Application {
    pub fn new() -> Rc<Self> {
        let app = gtk::Application::new(Some(config::APP_ID), gio::ApplicationFlags::HANDLES_OPEN).unwrap();

        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RefCell::new(Some(r));

        let window = SettingsWindow::new();

        let application = Rc::new(Self {
            app,
            window,
            sender,
            receiver
        });
        application.init();
        application
    }

    pub fn run(&self, app: Rc<Self>) -> i32 {
        let receiver = self.receiver.borrow_mut().take().unwrap();
        receiver.attach(None, move |action| app.do_action(action));

        let ret = self.app.run(&std::env::args().collect::<Vec<_>>());
        ret
    }

    fn init(&self) {
        self.setup_signals();

        // About
        action!(self.app, "about", clone!(@strong self.window.widget as window => move |_,_| {
            let builder = gtk::Builder::from_resource(&format!("{}about_dialog.ui", config::APP_PATH));
            get_widget!(builder, gtk::AboutDialog, about_dialog);

            about_dialog.set_transient_for(Some(&window));
            /*FIXME unsafe {
                about_dialog.connect_response(|dialog, _| dialog.destroy());
            }*/
            about_dialog.show();
        }));

        // Handlers list
        action!(self.app, "default_handlers", clone!(@strong self.window.widget as window, @strong self.sender as sender => move |_,_| {
            let handlers_window = HandlersWindow::new(sender.clone()).window();

            handlers_window.set_transient_for(Some(&window));
            handlers_window.show();
        }));
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        match action {
            Action::ChangeHandler(handler) => {
                self.window.change_handler(handler);
            }
        };
        glib::Continue(true)
    }

    fn setup_signals(&self) {
        let window = self.window.widget.clone();
        self.app.connect_activate(move |app| {
            window.set_application(Some(app));
            app.add_window(&window);
            window.present();
        });

        #[cfg(feature = "flatpak")]
        self.app.connect_open(move |app,urls,_str| {
            let v: Vec<String> = monitor_urls_in_gfiles(urls);
            let monitored_urls: Vec<&str> = v.iter().map(AsRef::as_ref).collect();

            let notif = gio::Notification::new(&gettext(config::APP_NAME));

            if !monitored_urls.is_empty() {
                let default_handler = general_settings::get_string(Key::DefaultHandler);
                let handlers_list = common::get_uri_handlers();

                if default_handler == "flat_open_uri" {
                    let connection = zbus::Connection::new_session();
                    if connection.is_ok() { // Log this ?
                        let open_uri_proxy = OpenURIProxy::new(&connection.unwrap()).unwrap(); // Log this ?
                        let option = OpenFileOptions {
                            handle_token: None,
                            writeable: None,
                            ask: Some(true)
                        };
                        open_uri_proxy.open_uri(WindowIdentifier::default(), monitored_urls.first().unwrap(), option).ok(); // Log this ?
                    }
                }
                else if default_handler == "none" || !handlers_list.contains_key(&default_handler) {
                    none_handler!(notif, app);
                }
                else {
                    handler_in_list!(default_handler, handlers_list, monitored_urls, notif, app);
                }
            }
            else {
                no_url!(notif, app);
            }
        });

        #[cfg(not(feature = "flatpak"))]
        self.app.connect_open(move |app,urls,_str| {
            let v: Vec<String> = monitor_urls_in_gfiles(urls);
            let monitored_urls: Vec<&str> = v.iter().map(AsRef::as_ref).collect();

            let notif = gio::Notification::new(&gettext(config::APP_NAME));

            if !monitored_urls.is_empty() {
                let default_handler = general_settings::get_string(Key::DefaultHandler);
                let handlers_list = common::get_uri_handlers();

                /*if default_handler == "clipboard" { //FIXME doesn't keep the clipboard after the application exit
                    let clipboard = gtk::Clipboard::get(&gdk::SELECTION_CLIPBOARD);
                    clipboard.set_text(monitored_urls.first().unwrap());
                    clipboard.store();

                    notif.set_body(Option::<&str>::Some(&gettext("Monitored URL was put in your clipboard")));
                    app.send_notification(Option::<&str>::Some("clipboard"), &notif);
                }
                else */

                if default_handler == "none" || !handlers_list.contains_key(&default_handler) {
                    none_handler!(notif, app);
                }
                else {
                    handler_in_list!(default_handler, handlers_list, monitored_urls, notif, app);
                }
            }
            else {
                no_url!(notif, app);
            }
        });
    }
}

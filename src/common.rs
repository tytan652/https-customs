use gtk::gio;
use gtk::prelude::*;
use std::collections::HashMap;
use std::path::PathBuf;

use crate::config::EXEC_CMD;

// May need refactor (but need to keep system order)
pub fn get_recommended_handlers() -> Vec<gio::AppInfo> {
    let mut handlers: Vec<gio::AppInfo> = Vec::new();

    let recommendeds = gio::AppInfo::get_recommended_for_type("x-scheme-handler/https");
    for recommended in recommendeds {
        if !(recommended.get_commandline().unwrap() == PathBuf::from(EXEC_CMD)) {
            handlers.push(recommended);
        }
    }

    handlers
}

pub fn get_other_handlers() -> Vec<gio::AppInfo> {
    let mut handlers: Vec<gio::AppInfo> = Vec::new();

    let rec_apps = gio::AppInfo::get_recommended_for_type("x-scheme-handler/https");

    let apps = gio::AppInfo::get_all();
    for app in apps {
        //Take apps who accept URI
        if app.supports_uris() && app.should_show() {

            //Skip duplicate with recommendeds and https-customs
            let mut not_add_it: bool = false;
            for rec_app in &rec_apps {
                if app.equal(rec_app) {
                    not_add_it = not_add_it | true;
                }
                else {
                    not_add_it = not_add_it | false;
                }
            }

            if !not_add_it {
                handlers.push(app);
            }
        }
    }

    handlers
}

pub fn get_uri_handlers() -> HashMap<String, gio::AppInfo> {
    let mut handlers: HashMap<String, gio::AppInfo> = HashMap::new();

    let apps = gio::AppInfo::get_all();
    for app in apps {
        if app.supports_uris() && app.should_show() {
            handlers.insert(app.get_commandline().unwrap().to_str().unwrap().to_string(), app);
        }
    }

    handlers
}

use gettextrs::*;

mod config;
mod static_resources;
mod common;
mod application;
mod ui;

use application::Application;

fn main() {
    //TODO setup a logger ?

    gtk::init().unwrap_or_else(|_| panic!("Failed to initialize GTK."));
    libadwaita::init();

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(config::GETTEXT_PACKAGE, config::LOCALEDIR);
    textdomain(config::GETTEXT_PACKAGE);

    // Load app resources
    static_resources::init().expect("Failed to initialize the resource file.");

    let app = Application::new();
    let ret = app.run(app.clone());
    std::process::exit(ret);
}

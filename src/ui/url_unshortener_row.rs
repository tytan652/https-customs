use gtk::prelude::*;
use gtk_macros::get_widget;
use https_customs::settings::{
    UrlUnshortenerKey,
    url_unshortener_settings
};

use crate::config::APP_PATH;

pub struct UrlUnshortenerRow {
}

impl UrlUnshortenerRow {
    pub fn new() -> libadwaita::ActionRow {

        let builder = gtk::Builder::from_resource(&format!("{}url_unshortener_row.ui", APP_PATH));
        let row: libadwaita::ActionRow = builder
            .get_object("row")
            .expect("Failed to find the row for UrlUnshortener");

        get_widget!(builder, gtk::Switch, switch);
        url_unshortener_settings::bind_property(UrlUnshortenerKey::Enabled, &switch, "state");

        row
    }
}

use gtk::prelude::*;
use gtk_macros::get_widget;
use https_customs::settings::frontend_changer_settings;

use crate::config::APP_PATH;


pub struct FrontEndChangerRow {
}

impl FrontEndChangerRow {
    pub fn new() -> libadwaita::ActionRow {

        let builder = gtk::Builder::from_resource(&format!("{}frontend_changer_row.ui", APP_PATH));
        let row: libadwaita::ActionRow = builder
            .get_object("row")
            .expect("Failed to find the row for FrontEndChanger");

        get_widget!(builder, gtk::Switch, switch);
        frontend_changer_settings::bind_enabled_property(&switch, "state", gtk::gio::SettingsBindFlags::DEFAULT);

        row
    }
}

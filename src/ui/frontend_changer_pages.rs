use gtk::prelude::*;
use gtk_macros::get_widget;
use https_customs::settings::{
    frontend_changer_settings,
    FrontEndChangerKey,
    FrontEndCommonKey,
    frontend_common_settings
};

use crate::config::APP_PATH;

macro_rules! bind_switch {
    ($builder:expr, $key:expr, $switch:ident) => {{
        get_widget!($builder, gtk::Switch, $switch);
        frontend_common_settings::bind_property($key, FrontEndCommonKey::Enabled, &$switch, "state");
    }};
}

macro_rules! bind_expander {
    ($builder:expr, $key:expr, $name:ident) => {{
        get_widget!($builder, libadwaita::ExpanderRow, $name);
        frontend_common_settings::bind_property($key, FrontEndCommonKey::UseFrontend, &$name, "enable-expansion");
    }};
}
//TODO: combine macros if OSM have instance selections
macro_rules! bind_entry {
    ($builder:expr, $key:expr) => {{
        get_widget!($builder, gtk::Entry, instance_entry);
        frontend_common_settings::bind_property($key, FrontEndCommonKey::FrontendInstance, &instance_entry, "text");
    }};
}

fn page_builder(name: &str) -> gtk::Builder {
    gtk::Builder::from_resource(&format!("{}frontend_changer_{}.ui", APP_PATH, name))
}

fn page(builder: &gtk::Builder, name: &str) -> libadwaita::StatusPage {
    let page: libadwaita::StatusPage = builder
        .to_owned()
        .get_object(name)
        .expect(&format!("Failed to find the {} page in frontend_changer_pages", name));

    page
}

pub struct FrontEndChangerPage {
}

impl FrontEndChangerPage {
    pub fn new() -> libadwaita::StatusPage {
        let builder = page_builder("main");
        let page = page(&builder, "main");

        bind_switch!(builder, FrontEndChangerKey::YouTube, yt_sw);
        bind_switch!(builder, FrontEndChangerKey::Twitter, twitter_sw);
        bind_switch!(builder, FrontEndChangerKey::Instagram, ig_sw);
        bind_switch!(builder, FrontEndChangerKey::Reddit, reddit_sw);
        bind_switch!(builder, FrontEndChangerKey::GoogleMaps, maps_sw);

        page
    }
}

pub struct YouTubePage {
}

impl YouTubePage {
    pub fn new() -> libadwaita::StatusPage {
        let builder = page_builder("youtube");
        let page = page(&builder, "youtube");

        bind_expander!(builder, FrontEndChangerKey::YouTube, use_frontend);
        bind_entry!(builder, FrontEndChangerKey::YouTube);

        page
    }
}

pub struct TwitterPage {
}

impl TwitterPage {
    pub fn new() -> libadwaita::StatusPage {
        let builder = page_builder("twitter");
        let page = page(&builder, "twitter");

        bind_expander!(builder, FrontEndChangerKey::Twitter, use_frontend);
        bind_entry!(builder, FrontEndChangerKey::Twitter);

        page
    }
}

pub struct InstagramPage {
}

impl InstagramPage {
    pub fn new() -> libadwaita::StatusPage {
        let builder = page_builder("instagram");
        let page = page(&builder, "instagram");

        bind_expander!(builder, FrontEndChangerKey::Instagram, use_frontend);
        bind_entry!(builder, FrontEndChangerKey::Instagram);

        page
    }
}

pub struct RedditPage {
}

impl RedditPage {
    pub fn new() -> libadwaita::StatusPage {
        let builder = page_builder("reddit");
        let page = page(&builder, "reddit");

        bind_expander!(builder, FrontEndChangerKey::Reddit, use_frontend);
        bind_entry!(builder, FrontEndChangerKey::Reddit);

        page
    }
}

pub struct MapsPage {
}

impl MapsPage {
    pub fn new() -> libadwaita::StatusPage {
        let builder = page_builder("maps");
        let page = page(&builder, "maps");

        bind_expander!(builder, FrontEndChangerKey::GoogleMaps, use_osm);

        page
    }
}

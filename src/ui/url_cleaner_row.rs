use gtk::prelude::*;
use gtk_macros::get_widget;
use https_customs::settings::{
    UrlCleanerKey,
    url_cleaner_settings
};

use crate::config::APP_PATH;

pub struct UrlCleanerRow {
}

impl UrlCleanerRow {
    pub fn new() -> libadwaita::ActionRow {

        let builder = gtk::Builder::from_resource(&format!("{}url_cleaner_row.ui", APP_PATH));
        let row: libadwaita::ActionRow = builder
            .get_object("row")
            .expect("Failed to find the row for UrlCleaner");

        get_widget!(builder, gtk::Switch, switch);
        url_cleaner_settings::bind_property(UrlCleanerKey::Enabled, &switch, "state");

        row
    }
}

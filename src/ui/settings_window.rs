use gtk::gio;
use gtk::prelude::*;
use gtk::glib::clone;
use gtk_macros::get_widget;
use gettextrs::*;
use std::path::PathBuf;
use https_customs::settings::{Key, general_settings};
#[cfg(feature = "frontend_changer")]
use https_customs::settings::frontend_changer_settings;

use crate::{
    config,
    common //TODO check if common is needed or if it should be renamed
};

#[cfg(feature = "url_cleaner")]
use crate::ui::UrlCleanerRow;
#[cfg(feature = "frontend_changer")]
use crate::ui::{
    FrontEndChangerRow,
    frontend_changer_pages::*
};
#[cfg(feature = "url_unshortener")]
use crate::ui::UrlUnshortenerRow;

pub struct SettingsWindow {
    builder: gtk::Builder,
    pub widget: libadwaita::ApplicationWindow
}

#[cfg(any(feature = "url_cleaner", feature = "frontend_changer", feature = "url_unshortener"))]
macro_rules! add_quick_row {
    ($setter:expr, $row:ty, $name:ident) => {{
        let $name = <$row>::new();
        $setter.add_quick_row(&$name);
    }};
}

#[cfg(feature = "frontend_changer")]
macro_rules! add_fec_page {
    ($setter:expr, $page:ty, $name:ident, $title:expr) => {{
        let $name = $setter.add_page(&<$page>::new(), &$title);
        frontend_changer_settings::bind_enabled_property(&$name, "visible", gio::SettingsBindFlags::GET);
    }};
}

macro_rules! handler_none {
    ($setter:expr) => {{
        $setter.set_handler(gio::Icon::new_for_string("action-unavailable-symbolic").unwrap(), &gettext("None"), config::HANDLER_NONE.to_string());
    }};
}

macro_rules! handler_app {
    ($setter:expr, $handler:expr, $handlers_list:expr) => {{
        let app_info = $handlers_list.get(&$handler).unwrap().to_owned();

        let icon = app_info
            .get_icon()
            .unwrap_or(gio::Icon::new_for_string("application-x-executable").unwrap());

        let title = app_info.get_display_name().unwrap();

        let path_buf = app_info
            .get_commandline().unwrap()
                              .to_str().unwrap()
                                       .to_string();

        $setter.set_handler(icon, &title, path_buf);
    }};
}

impl SettingsWindow {
    pub fn new() -> Self {

        let builder = gtk::Builder::from_resource(&format!("{}settings_window.ui", config::APP_PATH));
        let widget: libadwaita::ApplicationWindow = builder
            .get_object("settings_window")
            .expect("Failed to find the window object");

        let window = Self {
            builder,
            widget,
        };
        window.init();
        window

    }

    fn init(&self) {
        self.is_default();
        self.setup_widgets();

        #[cfg(feature = "url_cleaner")]
        add_quick_row!(self, UrlCleanerRow, url_cleaner_row);

        #[cfg(feature = "frontend_changer")]
        {
            add_quick_row!(self, FrontEndChangerRow, frontend_changer_row);

            add_fec_page!(self, FrontEndChangerPage, fec_page, gettext("Front-end changer"));
            add_fec_page!(self, YouTubePage, yt_page, &format!("\t{}", gettext("Youtube")));
            add_fec_page!(self, TwitterPage, twitter_page, &format!("\t{}", gettext("Twitter")));
            add_fec_page!(self, InstagramPage, ig_page, &format!("\t{}", gettext("Instagram")));
            add_fec_page!(self, RedditPage, reddit_page, &format!("\t{}", gettext("Reddit")));
            add_fec_page!(self, MapsPage, maps_page, &format!("\t{}", gettext("Google Maps")));

        }

        #[cfg(feature = "url_unshortener")]
        add_quick_row!(self, UrlUnshortenerRow, url_unshortener_row);
    }

    fn is_default(&self) {
        let http_app = gio::AppInfo::get_default_for_uri_scheme("http");
        let https_app = gio::AppInfo::get_default_for_uri_scheme("https");

        let mut is_default: bool = false;
        if http_app.is_some() && https_app.is_some() {
            let http_app = http_app.unwrap();
            let https_app = https_app.unwrap();
            if https_app.equal(&http_app) && (https_app.get_commandline().unwrap() == PathBuf::from(config::EXEC_CMD)) {
                is_default = true;
            }
        }

        if !is_default {
            get_widget!(self.builder, libadwaita::ActionRow, not_default);
            not_default.set_visible(true);
        }
    }

    fn setup_widgets(&self) {
        // We link the name field of handler_laber with the default handler property
        //TODO: get rid of it ?
        get_widget!(self.builder, gtk::Label, handler_label);
        general_settings::bind_property(Key::DefaultHandler, &handler_label, "name");
        self.change_handler(handler_label.get_name().unwrap().as_str().to_string());

        // Setup "Make default"
        get_widget!(self.builder, gtk::Button, make_default);
        get_widget!(self.builder, libadwaita::ActionRow, not_default);
        make_default.connect_clicked(clone!(@strong not_default => move |_| {
            let app_list = common::get_uri_handlers();
            let app = app_list.get(config::EXEC_CMD).unwrap().to_owned();

            let mut failure: u16 = 0;
            for scheme in &["x-scheme-handler/http", "x-scheme-handler/https"] {
                let result = app.set_as_default_for_type(scheme);
                if result.is_err() {
                    failure = failure + 1;
                    //TODO: log this ?
                }
            }

            if failure == 0 {
               not_default.set_visible(false);
            }
        }));

    }

    fn set_handler(&self, icon: gio::Icon, title: &str, path_buf: String) {
        get_widget!(self.builder, gtk::Image, handler_icon);
        handler_icon.set_from_gicon(&icon);
        get_widget!(self.builder, gtk::Label, handler_label);
        handler_label.set_label(title);
        general_settings::set_string(Key::DefaultHandler, path_buf); //TODO replace by widget name filed with a macro
    }

    #[cfg(feature = "flatpak")]
    pub fn change_handler(&self, handler: String) {
        let handlers_list = common::get_uri_handlers();

        if handler == config::HANDLER_FLAT_OPEN_URI {
            self.set_handler(gio::Icon::new_for_string("web-browser-symbolic").unwrap(), &gettext("Open URI"), config::HANDLER_FLAT_OPEN_URI.to_string());
        }
        else if handler == config::HANDLER_NONE || !handlers_list.contains_key(&handler) {
            handler_none!(self);
        }
        else {
            handler_app!(self, handler, handlers_list);
        }
    }

    #[cfg(not(feature = "flatpak"))]
    pub fn change_handler(&self, handler: String) {
        let handlers_list = common::get_uri_handlers();

        if handler == config::HANDLER_NONE || !handlers_list.contains_key(&handler) {
            handler_none!(self);
        }
        else {
            handler_app!(self, handler, handlers_list);
        }
    }


    #[cfg(any(feature = "url_cleaner", feature = "frontend_changer", feature = "url_unshortener"))]
    fn add_quick_row<P: IsA<gtk::ListBoxRow> + IsA<gtk::Widget>>(&self, row: &P) {
        get_widget!(self.builder, gtk::ListBox, quick_list);
        quick_list.insert(row, -1);
    }

    #[cfg(any(feature = "url_cleaner", feature = "frontend_changer", feature = "url_unshortener"))]
    fn add_page<P: IsA<gtk::Widget>>(&self, page: &P, title: &str) -> gtk::StackPage {
        get_widget!(self.builder, gtk::Stack, stack);
        stack.add_titled(page, Some(page.get_name().unwrap().as_str()), title)
    }
}

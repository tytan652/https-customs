use gtk::gio;
use gtk::prelude::*;
use libadwaita::prelude::*;
use gtk::glib::{Sender, clone};
use log::error;
use gtk_macros::{get_widget, send};
#[cfg(feature = "flatpak")]
use gettextrs::*;

use crate::{
    application::Action,
    config::APP_PATH,
    common
};

pub struct HandlersWindow {
    builder: gtk::Builder,
    sender: Sender<Action>,
    window: libadwaita::Window
}

impl HandlersWindow {
    pub fn new(sender: Sender<Action>) -> Self {

        let builder = gtk::Builder::from_resource(&format!("{}handlers_window.ui", APP_PATH));
        let window: libadwaita::Window = builder
            .get_object("window")
            .expect("Failed to find the window for HandlersWindow");

        let config = Self {
            builder,
            sender,
            window
        };
        config.setup_lists();
        config
    }

    pub fn window(&self) -> libadwaita::Window {
        self.window.clone()
    }

    fn setup_lists(&self) {
        get_widget!(self.builder, libadwaita::ActionRow, none);
        none.set_activatable(true);
        self.set_row_signal(&none);

        #[cfg(feature = "flatpak")]
        {
            get_widget!(self.builder, gtk::ListBox, owned);
            self.add_row(owned, &gettext("Open URI"), "flat_open_uri", "web-browser-symbolic");
        }

        get_widget!(self.builder, gtk::ListBox, recommended);
        let recommended_list = common::get_recommended_handlers();
        for app_info in recommended_list {
            self.add_app(app_info, &recommended);
        }

        get_widget!(self.builder, gtk::ListBox, others);
        let others_list = common::get_other_handlers();
        for app_info in others_list {
            self.add_app(app_info, &others);
        }
    }

    fn set_row_signal(&self, row: &libadwaita::ActionRow) {
        row.to_owned().connect_activated(clone!(@strong self.sender as sender, @strong self.window as window => move |row| {
            send!(sender, Action::ChangeHandler(row.get_name().unwrap().to_string()));
            unsafe {
                window.destroy();
            }
        }));
    }

    fn add_row(&self, list: gtk::ListBox, title: &str, path_buf: &str, icon_name: &str) {
        let builder = libadwaita::ActionRowBuilder::new();
        let row: libadwaita::ActionRow = builder
            .name(path_buf)
            .title(title)
            .icon_name(icon_name)
            .activatable(true)
            .visible(true)
            .build();

        self.set_row_signal(&row);
        list.insert(&row, -1);
    }

    fn add_app(&self, app_info: gio::AppInfo, list: &gtk::ListBox) {
        let title = app_info.get_display_name().unwrap();
        let path_buf = app_info
            .get_commandline().unwrap()
                              .to_str().unwrap()
                                       .to_string();
        let icon = app_info
            .get_icon()
            .unwrap_or(gio::Icon::new_for_string("application-x-executable").unwrap());
        let icon_name = gio::IconExt::to_string(&icon).unwrap();

        self.add_row(list.to_owned(), &title, &path_buf, icon_name.as_str());
    }
}

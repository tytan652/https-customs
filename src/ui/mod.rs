mod settings_window;
mod handlers_window;

#[cfg(feature = "url_cleaner")]
mod url_cleaner_row;

#[cfg(feature = "frontend_changer")]
mod frontend_changer_row;
#[cfg(feature = "frontend_changer")]
pub mod frontend_changer_pages;

#[cfg(feature = "url_unshortener")]
mod url_unshortener_row;

#[cfg(feature = "url_cleaner")]
use url_cleaner_row::UrlCleanerRow;
#[cfg(feature = "frontend_changer")]
use frontend_changer_row::FrontEndChangerRow;
#[cfg(feature = "url_unshortener")]
use url_unshortener_row::UrlUnshortenerRow;

pub use settings_window::SettingsWindow;
pub use handlers_window::HandlersWindow;

# Manage features for cargo
enabled_features = ''

foreach i : available_features
  if get_option(i)
    if enabled_features == ''
      enabled_features = ' ' + i
    else
      enabled_features = enabled_features + ',' + i
    endif
  endif
endforeach

global_conf = configuration_data()
global_conf.set_quoted('APP_ID', application_id)
global_conf.set_quoted('APP_PATH', application_path)
global_conf.set_quoted('PKGDATADIR', prefix / pkgdatadir)
global_conf.set_quoted('PROFILE', profile)
global_conf.set('NAME_SUFFIX', name_suffix)
global_conf.set_quoted('VERSION', version + version_suffix)
global_conf.set_quoted('GETTEXT_PACKAGE', gettext_package)
global_conf.set_quoted('LOCALEDIR', localedir)

config = configure_file(
    input: 'config.rs.in',
    output: 'config.rs',
    configuration: global_conf
)

# Copy the config.rs output to the source directory.
run_command(
  'cp',
  config,
  meson.current_source_dir(),
  check: true
)

# include_bytes! only takes a string literal
resource_conf = configuration_data()
resource_conf.set_quoted('RESOURCEFILE', resources.full_path())
resource_rs = configure_file(
  input: 'static_resources.rs.in',
  output: 'static_resources.rs',
  configuration: resource_conf
)

run_command(
  'cp',
  resource_rs,
  meson.current_source_dir(),
  check: true
)

sources = files(
  'config.rs',
  'main.rs',
  'static_resources.rs',
  'application.rs',
  'common.rs',
  'ui/mod.rs',
  'ui/settings_window.rs',
  'ui/handlers_window.rs',
  'ui/url_cleaner_row.rs',
  'ui/frontend_changer_row.rs',
  'ui/frontend_changer_pages.rs',
  'ui/url_unshortener_row.rs'
)

# TODO add to release the lib

if get_option('flatpak')
  enabled_features = enabled_features + ',flatpak'
endif

cargo_release = custom_target(
  'cargo-build',
  build_by_default: true,
  input: [sources, lib_sources],
  output: meson.project_name(),
  console: true,
  install: true,
  install_dir: bindir,
  depends: resources,
  command: [
    cargo_script,
    meson.build_root(),
    meson.source_root(),
    '@OUTPUT@',
    meson.project_name(),
    profile,
    enabled_features,
  ]
)
